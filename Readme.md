# Glamour Shots #

This is the Glamour Shots plugin, a plugin for use in the Dungeon Helper framework.

This plugin shows you the character generation options you picked for your facial features.

## Requirements ##
- dotnet-cli-zip - another dotnet tool:
```dotnet tool install --global dotnet-cli-zip --version 3.1.3```

## To make changes ##
- Increment the version in the project
- Add notes to the changelog for what was done.
- Push to a new branch, create a merge request.