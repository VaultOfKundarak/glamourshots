﻿namespace GlamourShots
{
    partial class IngameUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IngameUI));
            btnShowProperties = new System.Windows.Forms.Button();
            tbResults = new System.Windows.Forms.TextBox();
            SuspendLayout();
            // 
            // btnShowProperties
            // 
            btnShowProperties.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            btnShowProperties.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            btnShowProperties.Location = new System.Drawing.Point(12, 12);
            btnShowProperties.Name = "btnShowProperties";
            btnShowProperties.Size = new System.Drawing.Size(348, 36);
            btnShowProperties.TabIndex = 0;
            btnShowProperties.Text = "Extract Properties";
            btnShowProperties.UseVisualStyleBackColor = true;
            btnShowProperties.Click += btnShowProperties_Click;
            // 
            // tbResults
            // 
            tbResults.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            tbResults.Location = new System.Drawing.Point(12, 54);
            tbResults.Multiline = true;
            tbResults.Name = "tbResults";
            tbResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            tbResults.Size = new System.Drawing.Size(348, 306);
            tbResults.TabIndex = 1;
            // 
            // IngameUI
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackgroundImage = (System.Drawing.Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new System.Drawing.Size(372, 372);
            ControlBox = false;
            Controls.Add(tbResults);
            Controls.Add(btnShowProperties);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "IngameUI";
            ShowIcon = false;
            ShowInTaskbar = false;
            Text = "IngameUI";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button btnShowProperties;
        private System.Windows.Forms.TextBox tbResults;
    }
}