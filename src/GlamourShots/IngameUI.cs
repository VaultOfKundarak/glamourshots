﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using VoK.Sdk;
using VoK.Sdk.Common;
using VoK.Sdk.Dat;
using VoK.Sdk.Ddo;
using VoK.Sdk.Ddo.Enums;
using VoK.Sdk.Enums;
using VoK.Sdk.Properties;
using Windows.Devices.PointOfService;

namespace GlamourShots
{
    public partial class IngameUI : Form
    {
        private readonly IDdoGameDataProvider _provider;
        private readonly string _folder;
        private readonly IDatFile _clientGeneral;
        private readonly IDatFile _clientGamelogic;

        public IngameUI(IDdoGameDataProvider provider, string folder)
        {
            InitializeComponent();

            _provider = provider;
            _folder = folder;
            _clientGeneral = DatFactory.LoadDatFile(Path.Combine(_provider.ClientFolder, "client_general.dat"));
            _clientGamelogic = DatFactory.LoadDatFile(Path.Combine(_provider.ClientFolder, "client_gamelogic.dat"));
        }

        private void btnShowProperties_Click(object sender, EventArgs e)
        {
            try
            {
                var charId = _provider.GetCurrentCharacterId();
                if (charId == null || charId == 0) return;

                var sb = new StringBuilder();

                var props = _provider.GetCharacterBuildData(charId.Value);
                var charEntity = _provider.GetCurrentCharacter();
                var physicsObjId = charEntity?.PropertyCollection.GetInt32PropertyValue((uint)DdoProperty.PhysObj);
                if (physicsObjId == null || physicsObjId == 0) return;

                var raceType = props.GetEnumProperty((uint)DdoProperty.Creature_Species) as IEnumProperty;
                sb.AppendLine($"Race: {raceType.EnumString}");

                var gender = charEntity.WeeniePropertyCollection.GetEnumProperty((uint)DdoProperty.Character_Gender) as IEnumProperty;
                sb.AppendLine($"Gender: {gender.EnumString}");

                var physicsEntity = EntityDesc.Load(_clientGamelogic, (uint)physicsObjId.Value, _provider.PropertyMaster);
                var appearanceControls = physicsEntity.Properties.GetArrayProperty((uint)DdoProperty.AppearanceUI_Controls);

                var actualControls = appearanceControls.Properties.Where(p => p.PropertyId == (uint)DdoProperty.AppearanceUI_Control).Cast<IArrayProperty>();

                var appInstList = props.GetArrayProperty((uint)DdoProperty.Appearance_InstanceList);
                var appKeyInfos = appInstList?.Properties?.Where(p => p.PropertyId == (uint)DdoProperty.Appearance_KeyInfo)
                    .Cast<IArrayProperty>()
                    .ToList()
                    ?? new List<IArrayProperty>();

                foreach (var appearanceControl in actualControls)
                {
                    if (appearanceControl.Properties.Count < 1) continue;
                    var appearanceProps = appearanceControl.Properties.ToList();
                    string appearanceName = "";
                    string appearanceValue = "";

                    var firstId = appearanceProps[0].PropertyId;
                    if (firstId == (uint)DdoProperty.AppearanceUI_UIElementID)
                        firstId = appearanceProps[1].PropertyId;

                    if (firstId == (uint)DdoProperty.AppearanceUI_APRControl)
                    {
                        // need to load appearance table data
                        var aprControl = appearanceProps.FirstOrDefault(ap => ap.PropertyId == (uint)DdoProperty.AppearanceUI_APRControl) as IArrayProperty;
                        appearanceName = aprControl.GetStringInfoProperty((uint)DdoProperty.AppearanceUI_ControlStringName)?.Text;

                        var aprFile = aprControl.GetInt32PropertyValue((uint)DdoProperty.AppearanceUI_APRFile);
                        var charKeyInfo = appKeyInfos.FirstOrDefault(aki => aki.GetInt32PropertyValue((uint)DdoProperty.Appearance_AprFile) == aprFile);
                        var charValue = charKeyInfo.GetFloatPropertyValue((uint)DdoProperty.Appearance_Modifier);
                        var at = AppearanceTable.Load(_clientGeneral, (uint)aprFile.Value);
                        var aprKey = charKeyInfo.GetEnumProperty((uint)DdoProperty.Appearance_Key).UInt32Value;
                        var parts = at.Modifications[(int)aprKey].Parts.ToList();

                        var part = parts.FirstOrDefault(p => p.Intensity == charValue);
                        appearanceValue = $"{parts.IndexOf(part) + 1}";
                    }
                    else if (firstId == (uint)DdoProperty.AppearanceUI_CompositorControl)
                    {
                        var compositor = appearanceProps.FirstOrDefault(ap => ap.PropertyId == (uint)DdoProperty.AppearanceUI_CompositorControl) as IArrayProperty;
                        appearanceName = compositor.GetStringInfoProperty((uint)DdoProperty.AppearanceUI_ControlStringName)?.Text;

                        var controlPropId = compositor.GetUInt32PropertyValue((uint)DdoProperty.AppearanceUI_ControlPropertyName);
                        var valueProp = props.GetProperty(controlPropId.Value);

                        var value = valueProp.RawUInt32;
                        if (controlPropId.Value == (uint)DdoProperty.Av_Face_Detail_Variant)
                            value++;

                        appearanceValue = $"{value}";
                    }
                    else if (firstId == (uint)DdoProperty.AppearanceUI_DyeMapControl)
                    {
                        var dyemapControl = appearanceProps.FirstOrDefault(ap => ap.PropertyId == (uint)DdoProperty.AppearanceUI_DyeMapControl) as IArrayProperty;
                        appearanceName = dyemapControl.GetStringInfoProperty((uint)DdoProperty.AppearanceUI_ControlStringName)?.Text + " (ARGB)";

                        var controlPropId = dyemapControl.GetUInt32PropertyValue((uint)DdoProperty.AppearanceUI_ControlPropertyName);
                        var valueProp = props.GetProperty(controlPropId.Value);
                        appearanceValue = $"{valueProp}";
                    }

                    sb.AppendLine($"{appearanceName}: {appearanceValue}");

                }

                tbResults.Text = sb.ToString();
            }
            catch (Exception ex)
            {
                tbResults.Text = "Error extracting properties:\n" + ex.ToString();
            }
        }
    }
}
