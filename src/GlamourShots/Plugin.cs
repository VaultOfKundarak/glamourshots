﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoK.Sdk;
using VoK.Sdk.Ddo;
using VoK.Sdk.Plugins;

namespace GlamourShots
{
    public class Plugin : IDdoPlugin
    {
        // created one at https://www.guidgenerator.com/
        public Guid PluginId => Guid.Parse("6cfcfa71-4cd5-43c0-9c39-81df003236fb");

        public GameId Game => GameId.DDO;

        public string PluginKey => "50574f70-68d4-4e56-ba91-59284d5ba305";

        public string Name => "Glamour Shots";

        public string Description => "Exports character visual options from chargen screen";

        public string Author => "Morrikan";

        public Version Version => GetType().Assembly.GetName().Version;

        internal string Folder { get; set; }

        private PluginUI _ui;

        public static Plugin Instance { get; set; }

        public Plugin()
        {
            Instance = this;
        }

        public void Initialize(IDdoGameDataProvider gameDataProvider, string folder)
        {
            Folder = folder;
            _ui = new PluginUI(gameDataProvider, folder);
        }

        public IPluginUI GetPluginUI()
        {
            return _ui;
        }

        public void Terminate()
        {
        }
    }
}
